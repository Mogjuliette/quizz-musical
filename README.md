**Projet Quizz**

Ce quizz est un blind-test de musiques de jeux vidéo. S'inspirant de Kahoot, l'idée est de choisir entre plusieurs réponses possibles pour déterminer, à chaque nouvelle question, de quel jeu provient la musique entendue. J'ai trouvé des Stickers du site de Flaticon sur le thème de la musique. Je les ai utilisé et le visuel du reste du quizz en est tiré, en termes de gammes de couleurs.

Je souhaitais faire quelque chose de simple d'utilisation, qui fasse vraiment jeu. C'est dans cet esprit ludique que j'ai créé des animations pour les images. Pour mettre un peu de défi, le nombre de points obtenu dépend du temps mis à repondre à chaque question. Un compteur de bonnes réponses, un compteur de questions, et l'affichage des points servent à compléter l'expérience utilisateur. Pour indiquer la bonne réponse, j'ai choisi de passer l'arrière-plan des touches en rouge si la réponse est mauvaise et en vert si elle est bonne, car ces codes sont connus de tous.tes. 

D'un point de vue code, les réponses changent grâce à un tableau d'objet qui stocke pour chaque réponse l'url de l'image, de la musique, les réponses possibles et l'index de la bonne réponse. Le contenu du html est donc modifié par le javascript à chaque passage de la fonction, en fonction de l'index de la question actuelle, incrémenté à chaque tour. 

Pour le responsive, les tailles des éléments a été modifiée et l'affichage des boutons change grâce au système de colonnes de bootstrap. 

On pourrait aller plus loin en ajoutant différents thèmes au début du quizz, par exemple avoir le choix avec un quizz spécial jeux narratifs ou bien musiques vocales.

Le lien vers les wireframes de figma est disponible ici : https://www.figma.com/file/cVzURdYwfNaxVaBlWxWv5T/Quizz-musiques-de-jeux-vid%C3%A9ox?node-id=0%3A1&t=s3afmfKmk3SLboPO-1

Le site a été déployé sur Netlify à l'adresse suivante : https://inquisitive-creponne-6a3353.netlify.app/





**Consignes :**


Créer une application de type quizz/QCM/questionnaire sur le thème de votre choix (en gardant toujours en tête que
cette application pourrait être consultée par un·e employeur·se).

*Fonctionnalités obligatoire*

- Affichage des questions et choix de la réponse
- Enchaînement de plusieurs questions (sans changement de page)
- Décompte des points et affichage du score (en temps réel, et/ou à la fin)
- Responsive

Ne pas hésiter à rajouter d'autres fonctionnalités une fois celles ci implémentées (timer, plusieurs réponses possibles, réponse en champ de texte, illustrations pour certaines questions, etc.)

*Compétences à mobilisées*
Dans l'idéal, vous devrez utiliser des variables, conditions, tableaux, boucles et fonctions. Manipulation du DOM (querySelector / Events).
Essayer de maintenir votre code DRY.

*Réalisation*
Commencer par trouver le thème de votre quizz et une liste de questions pour celui ci.
Ensuite réaliser une ou plusieurs maquettes fonctionnelles de ce à quoi ressemblera l'application.
Vous devrez rendre un projet gitlab avec un README présentant le projet et les maquettes (et à terme pourquoi pas
une explication de votre méthodologie pour le code)
Commentez votre code avec au moins la JS doc de vos fonctions.