setTimeout(() => {
    echange()
    /**
     * Fonction permettant de supprimer le contenu de la section de départ et d'afficher le contenu de la section de consignes
     */
    function echange(){
        if(depart){
        depart.innerHTML="";
        }
        if(consignes){
        consignes.style.display="block";
        }
        /* console.log("Le fonction échange a échangé les sections début et consignes") */
    }
}, 2000);

buttonCommencer?.addEventListener("click",() => {
    console.log("Vous avez cliqué sur le bouton Commencer");
    
    passerAuxQuestions();
    setTimeout(() => {
    newCycleQuestion();    
    }, 1000)
   
    /* cycleQuestion(); */ 

}) 

/**
 * Fonction permettant d'effacer le contenu des deux premières sections pour pouvoir afficher la section avec les réponses
 */
function passerAuxQuestions() {
    if (buttonCommencer && depart) {
        depart.innerHTML = ""
    }
    if (buttonCommencer && consignes) {
        consignes.innerHTML = ""
        consignes.style.display = "none"
    }
    if (buttonCommencer && quizzQuestions) {
        quizzQuestions.style.display = 'block'
    }
}

/* passerAuxQuestions() */
/* cycleQuestion() */

function cycleQuestion() {
        console.log("La fonction cycleQuestion a été déclenchée");
        question(tabReponses,tabBonneReponse);
        changeImage(tabImages,indexQuestion);
        changeMusic(tabMusiques,indexQuestion);
        if (listButtons) {
        for (const button of listButtons) {
            button.classList.remove("clicked")
            /* console.log("for const buttons of ListButtons") */
            button.addEventListener('click', () => {
                console.log("Vous avez cliqué sur un bouton de réponse")
                button.classList.add("clicked")
                for (let indexButton = 0; indexButton < 4; indexButton++){
                    let itemButton = listButtons[indexButton];
                    
                    if (indexButton == tabBonneReponse[indexQuestion]) {
                        itemButton.style.backgroundColor = "green"
                        pointsCounter += 100
                        console.log(tabBonneReponse[indexQuestion])
                    } 
                    if (indexButton !== tabBonneReponse[indexQuestion] && itemButton.classList.contains("clicked")) {
                        itemButton.style.backgroundColor = "red"
                        /* console.log("Vous avez cliqué sur la mauvaise réponse d'index "+indexButton+"")
                        console.log("La bonne réponse était à l'index "+tabBonneReponse[indexQuestion]+"") */
                    }
                }
                setTimeout(() => {
                    console.log("fonction timeout déclenchée")
                    fondBleu();
                    IndexQuestionPlus();
                    console.log("top ");
                    
                    if(indexQuestion < 3){
                       return ;
                ;
                    } else {
                        console.log("fin du jeu")
                        return;
                    } 
                }, 1000)
            })
        }
    } 
}
/**
 * Permet de remettre tous les fonds des boutons de réponses en bleu
 */
/* function fondBleu() {
                        if (listButtons) {
                            for (const button of listButtons) {
                                button.style.backgroundColor = "#074785" 
                            } */
                        /* console.log("fonction fondBleu a changé les fonds des réponses en bleu"); */
                        /* }
                    } */
/**
 * Fonction permettant de changer l'image au-dessus de chaque question
 * @param srcImage Tableau des différentes urls/chemins d'accès des images
 * @param indexImage Correspond en fait à l'index de la question
 */
/* function changeImage(srcImage:string[],indexImage:number){
   if(questionImage){
        questionImage.src = srcImage[indexImage]; */
        /* console.log("fonction changeImage a changé l'image") */
 /*    } 
} */
/**
 * Fonction permettant de changer la musique à chaque question
 * @param srcMusic Tableau avec l'url de chaque musique
 * @param indexMusique Index de la question 
 */
/* function changeMusic(srcMusic:string[],indexMusique:number){
    if(questionMusic){
        questionMusic.src = srcMusic[indexMusique]; */
        /* console.log("fonction changeMusic a changé la musique") */
/*     }
} */

/**
 * Fonction permettant de changer les réponses de la nouvelle question et de dire laquelle est la bonne
 * @param Reponses Tableau des différentes réponses, à deux entrées
 * @param BonneReponse Tableau de l'index de chaque bonne réponse
 */
/* function question(Reponses:string[][],BonneReponse:number[]){
    
    if(button1&&){
      button1.innerHTML = Reponses[indexQuestion][0]
    }
    if(button2){
        button2.innerHTML = Reponses[indexQuestion][1]
      }
    if(button3){
        button3.innerHTML = Reponses[indexQuestion][2]
    }
    if(button4){
        button4.innerHTML = Reponses[indexQuestion][3]
    } */

    /* console.log("réponses changées avec fonction question") */
/*  } */

/*  function IndexQuestionPlus(){
    indexQuestion++;
    console.log("l'index des questions est maintenant égal à "+indexQuestion+"")
 } */


function newCycleQuestion() {
    for(let indexQuestion = 0; indexQuestion < 2; indexQuestion++){
        console.log(indexQuestion)
        
        if (questionMusic) {
            console.log("changement musique")
            questionMusic.src = tabMusiques[indexQuestion]
        }
        if (questionImage) {
            questionImage.src = tabImages[indexQuestion]
        }
        for (let i = 0; i < 4; i++) {
            if (listButtons) {
                listButtons[i].innerHTML = tabReponses[indexQuestion][i]
            }
        }
        for (let i = 0; i < 4; i++) {
            listButtons[i].addEventListener("click", () => {
                console.log("cliqué")
                setTimeout(() => {
                    console.log("timeout")
                    
                }, 1000)
            })
        }
    }
    return
}

/* setTimeout(() => {
    button1.style.background.color = ""#074785" "
}, 1000) */