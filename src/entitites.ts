export interface Question  {
    music:string,
    image:string,
    answers:string[],
    trueAnswer:number,
}