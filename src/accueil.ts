import { Question } from "./entitites";

import imgDisquette from "../public/image/disquette.png";
import imgHeadphones from "../public/image/headphones.png";
import imgCassette from "../public/image/cassette.png";
import imgBoombox from "../public/image/boombox.png";
import imgMug from "../public/image/mug.png";
import imgMusicalNote from "../public/image/musical-note.png";
import imgCD from "../public/image/cd.png";
import imgElectricKeyboard from "../public/image/electric-keyboard.png";
import imgLp from "../public/image/lp.png"
import imgSpeaker from "../public/image/speaker.png"


import music1 from "url:../public/audio/musique1.mp3";
import music2 from "url:../public/audio/musique2.mp3";
import music3 from "url:../public/audio/musique3_cut.mp3";
import music4 from "url:../public/audio/musique4.mp3";
import music5 from "url:../public/audio/musique5.mp3";
import music6 from "url:../public/audio/musique6.mp3";
import music7 from "url:../public/audio/musique7.mp3";
import music8 from "url:../public/audio/musique8.mp3";
import music9 from "url:../public/audio/musique9.mp3";
import music10 from "url:../public/audio/musique10.mp3";

console.log("Hello, début du script javasript, page index")

let buttonCommencer = document.querySelector<HTMLButtonElement>('#commencer') //boutton pour commencer
let depart = document.querySelector<HTMLElement>('.depart') //toute première section avec le logo
let consignes = document.querySelector<HTMLElement>('.consignes') //seconde section à s'afficher, avec les consignes du jeu

let quizzQuestions = document.querySelector<HTMLElement>('.quizz-questions') //Section générique avec image et quatre bouttons pour les questions

let questionImage = document.querySelector<HTMLImageElement>('#question-image')//L'image au dessus de chaque question
let questionMusic = document.querySelector<HTMLAudioElement>('#question-music')
//Les quatre boutons :
/* let button1 = document.querySelector<HTMLButtonElement>('#button1')
let button2 = document.querySelector<HTMLButtonElement>('#button2')
let button3 = document.querySelector<HTMLButtonElement>('#button3')
let button4 = document.querySelector<HTMLButtonElement>('#button4') */
let listButtons = document.querySelectorAll<HTMLButtonElement>('.list-buttons')

let counter = document.querySelector<HTMLSpanElement>('.counter')
let finalCounter = document.querySelector<HTMLSpanElement>('.final-counter')

let retour = document.querySelector<HTMLButtonElement>('.retour')
let timeStart = 0
let timeStop = 0
let timePoint = 0
let points = document.querySelector<HTMLDivElement>('.points')


let pointsCounter:number = 0 //le nombre de points
/* let indexQuestion = 0 //A quelle question on en est */

let indexQuestion = 0

let end = document.querySelector<HTMLElement>('.end')

let tabQuestions:Question[] = [
    {
        music:music1,
        image:imgDisquette,
        answers:["The Witcher 3","Heaven's Vault","Pyre","Slay the Spire"],
        trueAnswer:1,
    },

    {
        music:music2,
        image:imgHeadphones,
        answers:["Subnautica","Journey","Hollow Knight","Rayman"],
        trueAnswer:2,
    },

    {
        music:music3,
        image:imgCassette,
        answers:["Frostpunk","Beyond : two souls","Gnosia","Dishonored"],
        trueAnswer:1,
    },

    {
        music:music4,
        image:imgBoombox,
        answers:["Spiritfarer","Zelda","Final Fantasy VII","Rayman"],
        trueAnswer:3,
    },

    {   
        music:music5,
        image:imgMug,
        answers:["Final Fantasy IX","Gris","Child of Light","Tunic"],
        trueAnswer:2,
    },

    {   
        music:music6,
        image:imgMusicalNote,
        answers:["9 hours 9 persons 9 doors","Professor Layton","The Room","Danganronpa"],
        trueAnswer:0,
    },

    {   
        music:music7,
        image:imgCD,
        answers:["Va-11 Hall-A","2064 Read Only Memory","Tokyo Dark","Cyberpunk 2077"],
        trueAnswer:0,
    },

    {   
        music:music8,
        image:imgElectricKeyboard,
        answers:["Assassin's Creed","Elden Rings","Okami","The Witcher 3"],
        trueAnswer:3,
    },

    {   
        music:music9,
        image:imgLp,
        answers:["Drakengarg","Nier Automata","Prince of Persia","Bastion"],
        trueAnswer:1,
    },

    {   
        music:music10,
        image:imgSpeaker,
        answers:["Halo","Gris","Apex","Transistor"],
        trueAnswer:3,
    }

]



begin()

function begin() {
    setTimeout(() => {
        echange()
        /**
         * Fonction permettant de supprimer le contenu de la section de départ et d'afficher le contenu de la section de consignes
         */
        function echange() {
            if (depart) {
                depart.style.display = "none"
            }
            if (consignes) {
                consignes.style.display = "block"
            }
            /* console.log("Le fonction échange a échangé les sections début et consignes") */
        }
    }, 2000)

    buttonCommencer?.addEventListener("click", () => {
        console.log("Vous avez cliqué sur le bouton Commencer")

        afficherSectionQuestions()
        /**
        * Fonction permettant d'effacer le contenu des deux premières sections pour pouvoir afficher la section avec les réponses
        */
        function afficherSectionQuestions() {
            if (buttonCommencer && consignes) {

                consignes.style.display = "none"
            }
            if (buttonCommencer && quizzQuestions) {
                quizzQuestions.style.display = 'block'
            }
        }
        showQuestion()

    })
}

function showQuestion(){
    console.log("fonction showQuestion")
        if(questionMusic){
            questionMusic.src = tabQuestions[indexQuestion].music;
            
        }
        if(questionImage){
            questionImage.src = tabQuestions[indexQuestion].image;
            questionImage.classList.remove("img-animate");
            void questionImage.offsetHeight;
            questionImage.classList.add("img-animate")
        }
        for (const [i, button] of listButtons.entries()) {
            button.innerHTML=tabQuestions[indexQuestion].answers[i];
        }
        timeStart = Date.now()
        console.log("timeStart = "+timeStart)
    
    }

showResults()

function showResults(){

    function ifClicked(numButton:number, myButton:HTMLButtonElement){
                        console.log('fonction ifClicked')
                        
                        if(numButton == tabQuestions[indexQuestion].trueAnswer){
                            console.log('bravo')
                            myButton.style.backgroundColor = "green"
                            let timeDuration = timeStop - timeStart
                            timePoint = Math.trunc((60000 - timeDuration)/100 + 100)
                            pointsCounter += timePoint
                            console.log("durée = "+(timeStop - timeStart))
                            let para = document.createElement('p')
                            if(points){
                                points.appendChild(para)
                                para.innerHTML = `<span>Bravo ! +${timePoint} !</span>`
                                para.classList.add('grow')
                            }
                            /* if(quizzQuestions){
                                quizzQuestions.classList.add('disparaitre-animation')
                            } */
                            setTimeout(() => {
                            myButton.style.backgroundColor = "#074785";
                            if (counter){
                                counter.innerHTML = "Dommage !";
                            }
                            /* if(quizzQuestions){
                                quizzQuestions.classList.remove('disparaitre-animation')
                            } */
                            changeQuestion()
                            ;
                            }, 2000)
                        }
                        if(numButton !== tabQuestions[indexQuestion].trueAnswer){
                            console.log('dommage')
                            myButton.style.backgroundColor = "red"
                            listButtons[tabQuestions[indexQuestion].trueAnswer].style.backgroundColor = "green";
                            let para = document.createElement('p')
                            if(points){
                                points.appendChild(para)
                                para.innerHTML = `<span>Dommage !</span>`
                                para.classList.add('grow')
                            }
                            setTimeout(() => {
                            myButton.style.backgroundColor = "#074785";
                            listButtons[tabQuestions[indexQuestion].trueAnswer].style.backgroundColor = "#074785";
                            changeQuestion();
                            }, 2000)
                        }
                        
                    }
        
        
        for(let index = 0; index < 4; index++){
            listButtons[index].addEventListener('click', () => {
                console.log("bouton"+index+"cliqué");
                timeStop = Date.now()
                console.log("timeStop = "+timeStop)
                if(listButtons[index]){
                    ifClicked(index, listButtons[index]);
                }
            })
        }
}

function changeQuestion(){
    indexQuestion++;

   
    if(points){
        points.innerHTML = ""
    }
    console.log("fonction changeQuestion, indexQuestion est désormais à "+indexQuestion+"")
    /* if(quizzQuestions){
        quizzQuestions.classList.remove('disparaitre-animation') */
    
    
    

    if (indexQuestion < tabQuestions.length){
        console.log("le jeu n'est pas fini");
        showQuestion();
    } else {
        console.log("le jeu est fini");
        if(quizzQuestions){
            quizzQuestions.style.display = 'none';
        }
        if(questionMusic){
            questionMusic.src="";
        }
        if(end){
            end.style.display = 'block';
            if(finalCounter){
                finalCounter.innerHTML = String(pointsCounter);
            }
        }
        return
    }
}

retour?.addEventListener('click', () => {
    if(end){
        end.style.display = 'none';
    }
    pointsCounter = 0;
    indexQuestion = 0;
    if (depart) {
        depart.style.display = "block"
    }
    begin(); 
} 
)