//Importation des interfaces, des images et des musiques//

import { Question } from "./entitites";

import imgDisquette from "../public/image/disquette.png";
import imgHeadphones from "../public/image/headphones.png";
import imgCassette from "../public/image/cassette.png";
import imgBoombox from "../public/image/boombox.png";
import imgMug from "../public/image/mug.png";
import imgMusicalNote from "../public/image/musical-note.png";
import imgCD from "../public/image/cd.png";
import imgElectricKeyboard from "../public/image/electric-keyboard.png";
import imgLp from "../public/image/lp.png"
import imgSpeaker from "../public/image/speaker.png"
import imgCassette1 from "../public/image/cassette-tape_1.png"
import imgVideo from "../public/image/video.png";
import imgVinyl from "../public/image/vinyl.png";
import imgWalkman from "../public/image/walkman.png";
import imgCassetteTape from "../public/image/cassette-tape.png";
import imgGamepad1 from "../public/image/gamepad_1.png";
//Images déjà dans le html : gamepad, cassette-only, singer

import music1 from "url:../public/audio/musique1.mp3";
import music2 from "url:../public/audio/musique2.mp3";
import music3 from "url:../public/audio/musique3_cut.mp3";
import music4 from "url:../public/audio/musique4.mp3";
import music5 from "url:../public/audio/musique5.mp3";
import music6 from "url:../public/audio/musique6.mp3";
import music7 from "url:../public/audio/musique7.mp3";
import music8 from "url:../public/audio/musique8.mp3";
import music9 from "url:../public/audio/musique9.mp3";
import music10 from "url:../public/audio/musique10.mp3";
import music11 from "url:../public/audio/musique11_cut.mp3";
import music12 from "url:../public/audio/musique12.mp3";
import music13 from "url:../public/audio/musique13.mp3";
import music14 from "url:../public/audio/musique14.mp3";
import music15 from "url:../public/audio/musique15.mp3";
import music16 from "url:../public/audio/musique16_cut.mp3";

//Définition des différentes variables du DOM à partir de querySelectors

let buttonCommencer = document.querySelector<HTMLButtonElement>('#commencer') //boutton pour commencer
let depart = document.querySelector<HTMLElement>('.depart') //toute première section avec le logo
let consignes = document.querySelector<HTMLElement>('.consignes') //seconde section à s'afficher, avec les consignes du jeu
let quizzQuestions = document.querySelector<HTMLElement>('.quizz-questions') //Section générique avec image et quatre bouttons pour les questions
let questionImage = document.querySelector<HTMLImageElement>('#question-image')//L'image au dessus de chaque question
let questionMusic = document.querySelector<HTMLAudioElement>('#question-music')
let listButtons = document.querySelectorAll<HTMLButtonElement>('.list-buttons')
let counter = document.querySelector<HTMLSpanElement>('.counter')
let finalCounter = document.querySelector<HTMLSpanElement>('.final-counter')
let retour = document.querySelector<HTMLButtonElement>('.retour')
let sectionPoints = document.querySelector<HTMLDivElement>('.section-points')
let compteurPara = document.querySelector<HTMLParagraphElement>('.compteur-questions')
let pointsPara = document.querySelector<HTMLParagraphElement>('.points-para')
let goodPara = document.querySelector<HTMLParagraphElement>('.compteur-bonnes-réponses')
let end = document.querySelector<HTMLElement>('.end')
let endGoodOnes = document.querySelector<HTMLElement>('.end-good-ones')

//Définitions des variables non issues du DOM

let goodOnes = 0 //Le nombre de bonnes réponses
let pointsCounter = 0 //le nombre de points
let indexQuestion = 0//A quelle question on en est
let timeStart = 0 //Le temps quand la question démarre
let timeStop = 0 //Le temps lorsque l'on a répondu à la question
let timePoint = 0 //Le nombre de points lié au temps
let clickable = true //Active ou désactive les boutons de réponses

//Le tableau d'objets des questions

let tabQuestions:Question[] = [
    {
        music:music1,
        image:imgDisquette,
        answers:["The Witcher 3","Heaven's Vault","Pyre","Slay the Spire"],
        trueAnswer:1,
    },

    {
        music:music2,
        image:imgHeadphones,
        answers:["Subnautica","Journey","Hollow Knight","Rayman"],
        trueAnswer:2,
    },

    {
        music:music3,
        image:imgCassette,
        answers:["Frostpunk","Beyond : two souls","Gnosia","Dishonored"],
        trueAnswer:1,
    },

    {
        music:music4,
        image:imgBoombox,
        answers:["Spiritfarer","Zelda","Final Fantasy VII","Rayman"],
        trueAnswer:3,
    },

    {   
        music:music5,
        image:imgMug,
        answers:["Final Fantasy IX","Gris","Child of Light","Tunic"],
        trueAnswer:2,
    },

    {   
        music:music6,
        image:imgMusicalNote,
        answers:["9 hours 9 persons 9 doors","Professor Layton","The Room","Danganronpa"],
        trueAnswer:0,
    },

    {   
        music:music7,
        image:imgCD,
        answers:["Va-11 Hall-A","2064 Read Only Memory","Tokyo Dark","Cyberpunk 2077"],
        trueAnswer:0,
    },

    {   
        music:music8,
        image:imgElectricKeyboard,
        answers:["Assassin's Creed","Elden Rings","Okami","The Witcher 3"],
        trueAnswer:3,
    },

    {   
        music:music9,
        image:imgLp,
        answers:["Drakengarg","Nier Automata","Prince of Persia","Bastion"],
        trueAnswer:1,
    },

    {   
        music:music10,
        image:imgSpeaker,
        answers:["Halo","Gris","Apex","Transistor"],
        trueAnswer:3,
    },

    {   
        music:music11,
        image:imgCassette1,
        answers:["Endless Space 2","Stellaris","Surviving Mars","Subnautica"],
        trueAnswer:3,
    },
    
    {   
        music:music12,
        image:imgVideo,
        answers:["Undertale","Deltarune","Pokemon rouge et bleu","Professeur Layton"],
        trueAnswer:0,
    },
    
    {   
        music:music13,
        image:imgVinyl,
        answers:["Civilisation 4","Age of Empire II","Assassin's creed I","Final Fantasy VI"],
        trueAnswer:1,
    },
    
    {   
        music:music14,
        image:imgWalkman,
        answers:["What remains of Edith Finch","Portal 2","Long live the Queen","Haven"],
        trueAnswer:2,
    },
    
    {   
        music:music15,
        image:imgCassetteTape,
        answers:["Final Fantasy VII","Final Fantasy X","Final Fantasy IX","Final Fantasy VIII"],
        trueAnswer:1,
    },
    
    {   
        music:music16,
        image:imgGamepad1,
        answers:["Life is strange","Evoland 2","Frostpunk","Firewatch"],
        trueAnswer:2,
    } 
]

console.log("test")
begin()

/**
 * Fonction permettant de commencer le jeu lorsqu'il est lancé pour la première fois. Elle comporte l'enchaînement de trois sous fonctions : echange(), afficherSectionQuestions() et showQuestions()
 */
function begin() {
    setTimeout(() => {
        echange()
        /**
         * Fonction permettant de supprimer le contenu de la section de départ et d'afficher le contenu de la section de consignes
         */
        function echange() {
            if (depart) {
                depart.style.display = "none"
            }
            if (consignes) {
                consignes.style.display = "block"
            }
            /* console.log("Le fonction échange a échangé les sections début et consignes") */
        }
    }, 2000)

    buttonCommencer?.addEventListener("click", () => {
        afficherSectionQuestions()
        /**
        * Fonction permettant d'effacer le contenu des deux premières sections pour pouvoir afficher la section avec les réponses
        */
        function afficherSectionQuestions() {
            if (consignes) {

                consignes.style.display = "none"
            }
            if (quizzQuestions) {
                quizzQuestions.style.display = 'block'
            }

            if(sectionPoints){
                sectionPoints.style.display = "block"
            }
        }
        showQuestion()

    })
}

/**
 * Fonction permettant de montrer le bon contenu pour chaque question (une image, une musique et les quatre réponses possibles)
 */
function showQuestion(){
        if (compteurPara){
            compteurPara.innerHTML = `Question ${indexQuestion+1} / ${tabQuestions.length}`
        }
        if (goodPara){
        goodPara.innerHTML = `Bonnes réponses :  ${goodOnes}`
        }
        if(questionMusic){
            questionMusic.src = tabQuestions[indexQuestion].music  
        }
        if(questionImage){
            questionImage.src = tabQuestions[indexQuestion].image;
            questionImage.classList.remove("img-animate");
            void questionImage.offsetHeight;
            questionImage.classList.add("img-animate")
        }
        for (const [i, button] of listButtons.entries()) {
            button.innerHTML=tabQuestions[indexQuestion].answers[i];
        }
        clickable = true;
        timeStart = Date.now();
    }

showResults()

/**
 * Permet d'afficher si la réponse choisie est juste ou fausse, la bonne réponse, et les points.
 */
function showResults(){

    function ifClicked(numButton:number, myButton:HTMLButtonElement){
                        if(numButton == tabQuestions[indexQuestion].trueAnswer){
                            goodOnes++
                            myButton.style.backgroundColor = "green"
                            let timeDuration = timeStop - timeStart
                            timePoint = Math.trunc((60000 - timeDuration)/100 + 100)
                            pointsCounter += timePoint
                            if(pointsPara){
                                pointsPara.innerHTML = `<span>Bravo ! +${timePoint} !</span>`
                                pointsPara.classList.add('grow')
                            }
                            if(goodPara){
                                goodPara.innerHTML = `Bonnes réponses :  ${goodOnes}`
                            }
                            setTimeout(() => {
                            myButton.style.backgroundColor = "#074785";
                            if (counter){
                                counter.innerHTML = "Dommage !";
                            }
                            changeQuestion()
                            ;
                            }, 2000)
                        }
                        if(numButton !== tabQuestions[indexQuestion].trueAnswer){
                            myButton.style.backgroundColor = "red"
                            listButtons[tabQuestions[indexQuestion].trueAnswer].style.backgroundColor = "green";
                            if(pointsPara){
                                pointsPara.classList.remove("grow");
                                void pointsPara.offsetHeight;
                                pointsPara.classList.add("grow")
                                pointsPara.innerHTML = `<span>Dommage !</span>`
                                pointsPara.classList.add('grow')
                            }
                            setTimeout(() => {
                            myButton.style.backgroundColor = "#074785";
                            listButtons[tabQuestions[indexQuestion].trueAnswer].style.backgroundColor = "#074785";
                            changeQuestion();
                            }, 2000)
                        }
                        
                    }
        
        
        for(let index = 0; index < 4; index++){
            listButtons[index].addEventListener('click', () => {
                if(clickable == true){
                    clickable = false;
                    timeStop = Date.now();
                    if(listButtons[index]){
                        ifClicked(index, listButtons[index]);
                    } 
                } 
            })
        }
}

/**
 * Permet de changer de question s'il y a encore des questions, de ne plus afficher les points gagnés et de passer à l'écran de fin à la fin du quizz
 * @returns Sort de la fonction
 */
function changeQuestion(){
    indexQuestion++;
    if(pointsPara){
        pointsPara.innerHTML = ""
    }
    if (indexQuestion < tabQuestions.length){
        showQuestion();
    } else {
        if(quizzQuestions){
            quizzQuestions.style.display = 'none';
        }
        if(sectionPoints){
            sectionPoints.style.display = "none"
        }
        if(questionMusic){
            questionMusic.src="";
        }
        if(end){
            end.style.display = 'block';
            if(finalCounter){
                finalCounter.innerHTML = String(pointsCounter);
            }
            if(endGoodOnes){
                endGoodOnes.innerHTML = `Vous avez eu ${goodOnes} bonnes réponses sur ${tabQuestions.length}`
            }
        }
        return
    }
}

/*Gestion du bouton retour permettant de revenir au début du quizz */
retour?.addEventListener('click', () => {
    if(end){
        end.style.display = 'none';
    }
    pointsCounter = 0;
    indexQuestion = 0;
    goodOnes = 0;
    if (depart) {
        depart.style.display = "block"
    }
    begin(); 
} 
)